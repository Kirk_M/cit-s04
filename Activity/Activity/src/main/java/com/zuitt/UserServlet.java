package com.zuitt;

import jakarta.servlet.http.HttpServlet;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;

	
	public void init() throws ServletException {   
        System.out.println("******************************************");
        System.out.println(" InformationServlet has been initialized. ");
        System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		// Get the form data
        String fname = req.getParameter("fname");
        String lname = req.getParameter("lname");
        String email = req.getParameter("email");
        String contactNumber = req.getParameter("contactNumber");

        
        //Store First Name to System Properties.
        System.getProperties().put("fname",fname);
        
        
        //Store Last Name in HttpSession.
        HttpSession session = req.getSession();
        session.setAttribute("lname", lname);
        
        
        //Store Email in the Servlet COntext Attribute
        getServletContext().setAttribute("email", email);

        
        // Url Rewriting via sendRedirect Method
        res.sendRedirect("details?contactNumber="+contactNumber);
		
		
	}
	
	public void destroy() {

		
		System.out.println("***************************");
		System.out.println("Information has been Destroy.");
		System.out.println("***************************");
		
	}


}

